require 'redis'
require 'json'

class Redis
	# Redis Class
	@redis = Redis.new(url: @url, sentinels: @SENTINELS, role: @role)
	@url = nil
	@SENTINELS = nil
	@role = nil
	def initialize(URL, SENTINELS, role=:master)
		@url = URL
		@SENTINELS = SENTINELS
		@role = role
	end
	# Redis set
	# @param key
	#	Key of the value
	# @param value
	# 	Value to be set
	def set(key, value)
		@redis.set(key, value)
	end
	# Redis set but value is converted to JSON
	def setJSON (key, &block)
		@redis.set(key,&block.to_json)
	end
	# Redis get
	def get(key)
		return @redis.get(key)
	end
	# Redis get but value is converted from JSON
	def getJSON(key)
		return JSON.parse(@redis.get(key))
	end
end