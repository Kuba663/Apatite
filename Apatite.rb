require 'rack'
require_relative 'Data/Redis.rb'
require_relative 'Action.rb'

class Apatite
	# Initialize application
	def initialize(url=nil,senitels=nil,role=nil)
		@routes = {}
		@redis = Redis.new(:url => url,:senitels => senitels,:role => role)
	end
	# Add Route
	# @param http_method
	# 	GET, POST, PUT, DELETE, PATCH
	# @param path
	# 	url path
	# @param redirects
	# 	HTML meta location
	# @param block
	# 	Route content goes here
	def add_route(http_method, path, redirects={}, &block)
		response = Rack::Response.new
		response.body = block
		if redirects[:location]
			response.headers[“Location”] = redirects[:location]
			response.status = ‘302’
		end
		@routes[[http_method, path]] = [response.status, response.headers,  [response.body]]
	end
end