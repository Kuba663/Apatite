require 'rake'
Gem::Specification.new do |s|
s.files = FileList['Apatite.rb','Action.rb','Data/*'].to_a
s.name = 'Apatite'
s.summary = 'Apatite web framework'
s.version = '0.0.2.pre'
s.authors = ['VirtualFox']
s.licenses = ['NGPL']
s.add_runtime_dependency 'redis', '~> 4.0', '>= 4.0.1'
s.add_runtime_dependency 'rack', '~> 2.0', '>= 2.0.4'
s.metadata = {
	"source_code_uri"   => "https://gitlab.com/Kuba663/Apatite.git",
	"source_code_ssh"	=> "git@gitlab.com:Kuba663/Apatite.git",
	"wiki_uri"          => "https://gitlab.com/Kuba663/Apatite/wikis/",
	"bug_tracker"		=> "https://gitlab.com/Kuba663/Apatite/issues"
}
end